/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author digu
 */

import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;

class client {

    private Socket socket = null;
    private InputStream inStream = null;
    private OutputStream outStream = null;

    public client() {

    }

    public void createSocket(String host, int port,javax.swing.JTextArea msgArea,StringBuilder str,int status,String pathc,String paths) {
        try {
            socket = new Socket(host, port);
            System.out.println("Connected");
            inStream = socket.getInputStream();
            outStream = socket.getOutputStream();
            createReadThread(msgArea,str);
            createWriteThread(status,pathc,paths);
        } catch (UnknownHostException u) {
            u.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public void createReadThread(javax.swing.JTextArea msgArea,StringBuilder str) {
        Thread readThread = new Thread() {
            public void run() {
                while (socket.isConnected()) {

                    try {
                        byte[] readBuffer = new byte[200];
                        int num = inStream.read(readBuffer);
                        if (num > 0) {
                            byte[] arrayBytes = new byte[num];
                            System.arraycopy(readBuffer, 0, arrayBytes, 0, num);
                            String recvedMessage = new String(arrayBytes, "UTF-8");
                            str.append("Other: " +recvedMessage+"\n");
                            msgArea.setText(str.toString());
                        }
                        // else {
                        // 	nofify() ;
                        // }
                    } catch (SocketException se) {
                        System.exit(0);

                    } catch (IOException i) {
                        i.printStackTrace();
                    }

                }
            }
        };
        readThread.setPriority(Thread.MAX_PRIORITY);
        readThread.start();
    }

    public void createWriteThread(int status,String pathc,String paths) {
        
        String path;
        if(status == 1) {
            path = pathc ;
        }
        else {
            path = paths ;
        }
        Thread writeThread = new Thread() {
            public void run() {
               try {
                   BufferedReader br = new BufferedReader(new FileReader(path)) ;
                while (socket.isConnected()) {
                        
                        sleep(100);
                        String typedMessage = br.readLine() ;
                        
                        if (typedMessage != null && typedMessage.length() > 0) {
                            synchronized (socket) {
                                outStream.write(typedMessage.getBytes("UTF-8"));
                                sleep(100);
                            }
                        }

                   

                }
               }
               catch(Exception e) {
                   
               }
            }
        };
        writeThread.setPriority(Thread.MAX_PRIORITY);
        writeThread.start();
    }

}

class server {

    private ServerSocket serverSocket = null;
    private Socket socket = null;
    private InputStream inStream = null;
    private OutputStream outStream = null;

    public server() {

    }

    public ServerSocket createSocket(int port,javax.swing.JTextArea msgArea,StringBuilder str,int status,String pathc,String paths) {
        try {
            serverSocket = new ServerSocket(port);
            
            System.out.println("connected");
                socket = serverSocket.accept();
                inStream = socket.getInputStream();
                outStream = socket.getOutputStream();
                
                createReadThread(msgArea,str);
                createWriteThread(status,pathc,paths);

            
        } catch (IOException io) {
            io.printStackTrace();
        }
        return serverSocket ;
    }

    public void createReadThread(javax.swing.JTextArea msgArea,StringBuilder str) {
        Thread readThread = new Thread() {
            public void run() {
                while (socket.isConnected()) {
                    try {
                        byte[] readBuffer = new byte[200];
                        int num = inStream.read(readBuffer);

                        if (num > 0) {
                            byte[] arrayBytes = new byte[num];
                            System.arraycopy(readBuffer, 0, arrayBytes, 0, num);
                            String recvedMessage = new String(arrayBytes, "UTF-8");
                            str.append("Other: " +recvedMessage+"\n");
                            msgArea.setText(str.toString());
                        }
                        // else {
                        // 	notify();
                        // }

                    } catch (SocketException se) {
                        System.exit(0);

                    } catch (IOException i) {
                        i.printStackTrace();
                    }

                }
            }
        };
        readThread.setPriority(Thread.MAX_PRIORITY);
        readThread.start();
    }

    public void createWriteThread(int status,String pathc,String paths) {
        String path;
        if(status == 1) {
            path = pathc;
        }
        else {
            path = paths ;
        }
        Thread writeThread = new Thread() {
            
            public void run() {
                try {
                BufferedReader br = new BufferedReader(new FileReader(path)) ;
                while (socket.isConnected()) {
                    
                        sleep(100);
                        
                        String typedMessage = br.readLine() ;
                        
                        if (typedMessage != null && typedMessage.length() > 0) {
                            synchronized (socket) {
                                outStream.write(typedMessage.getBytes("UTF-8"));
                                sleep(100);
                            }
                        }

                    } 
                }
                catch(Exception e) {
                    System.out.println(e);
                }
            }
            
        };
        writeThread.setPriority(Thread.MAX_PRIORITY);
        writeThread.start();

    }
}


public class maingui extends javax.swing.JFrame {

    public maingui() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBox2 = new javax.swing.JCheckBox();
        jScrollBar1 = new javax.swing.JScrollBar();
        ipBox = new javax.swing.JTextField();
        serverSelection = new javax.swing.JRadioButton();
        clientSelection = new javax.swing.JRadioButton();
        portBox = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        msgLine = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        msgArea = new javax.swing.JTextArea();
        connetButton = new javax.swing.JButton();
        sendButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        jCheckBox2.setText("jCheckBox2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        serverSelection.setText("Server");
        serverSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                serverSelectionActionPerformed(evt);
            }
        });

        clientSelection.setText("Client");

        portBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                portBoxActionPerformed(evt);
            }
        });

        jLabel1.setText("IP");

        jLabel2.setText("Port");

        msgLine.setBackground(new java.awt.Color(232, 189, 241));
        msgLine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                msgLineActionPerformed(evt);
            }
        });

        msgArea.setBackground(new java.awt.Color(144, 187, 230));
        msgArea.setColumns(20);
        msgArea.setRows(5);
        jScrollPane1.setViewportView(msgArea);

        connetButton.setText("Start");
        connetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connetButtonActionPerformed(evt);
            }
        });

        sendButton.setText("Send");
        sendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendButtonActionPerformed(evt);
            }
        });

        jButton1.setText("X");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(clientSelection)
                    .addComponent(serverSelection))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(ipBox)
                    .addComponent(portBox, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(connetButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addGap(39, 39, 39))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(msgLine, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(sendButton)
                        .addGap(7, 7, 7)))
                .addGap(25, 25, 25))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(serverSelection)
                    .addComponent(ipBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(clientSelection)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(portBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 1, Short.MAX_VALUE)
                        .addComponent(connetButton, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(2, 2, 2)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(msgLine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sendButton))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void serverSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_serverSelectionActionPerformed
       
    }//GEN-LAST:event_serverSelectionActionPerformed

    private void msgLineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_msgLineActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_msgLineActionPerformed

    private void portBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_portBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_portBoxActionPerformed

    private void connetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connetButtonActionPerformed
        // TODO add your handling code here:
        
        Boolean isServer = serverSelection.isSelected() ;
        Boolean isClient = clientSelection.isSelected() ;
        int port ;
        String host ;
        if(isServer && isClient) {
            JOptionPane.showMessageDialog(this,"Select server or client", "Both cann't be selected", 2);
        }
        host = ipBox.getText().trim();
        port = Integer.valueOf(portBox.getText().trim());
        str = new StringBuilder("") ;
        try {
            fileWriter = new FileWriter(pathc);
            PrintWriter pr  = new PrintWriter(fileWriter) ;
            pr.println("");
            pr.close();
            
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        try {
            fileWriter = new FileWriter(paths);
            PrintWriter pr  = new PrintWriter(fileWriter) ;
            pr.println("");
            pr.close();
            
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        
        if(isClient) {
            client c = new client() ;
            status = 1 ;
            c.createSocket(host, port,msgArea,str,status,pathc,paths);
        }
        else {
            status = 2 ;
            server s = new server() ;
            sock = s.createSocket(port,msgArea,str,status,pathc,paths);
        }
    }//GEN-LAST:event_connetButtonActionPerformed

    private void sendButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendButtonActionPerformed
        // TODO add your handling code here:
        
        try {
            if(status == 1) fileWriter = new FileWriter(pathc, true);
            else fileWriter = new FileWriter(paths, true);
            Writer output;
            output = new BufferedWriter(fileWriter);
            PrintWriter pr  = new PrintWriter(output) ;
            String msg = msgLine.getText() ;
            
            pr.append(msg);
            pr.close();
            str.append("Me: " + msg +"\n") ;
            msgArea.setText(str.toString());
            
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        msgLine.setText("");
    }//GEN-LAST:event_sendButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {
            if(sock != null) {
                sock.close();
            }
            else {
                System.out.println("shit");
            }
        }
        catch(Exception e) {
            
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        os = System.getProperty("os.name");
        if(os.equals("Linux")) {
            pathc = "file1.txt" ;
            paths = "file2.txt" ;
                    
        }
        else {
            pathc = "X:\\file1.txt" ;
            paths = "X:\\file2.txt" ;
        }
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(maingui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(maingui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(maingui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(maingui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new maingui().setVisible(true);
            }
        });
    }
    
    
    private String ref1 ;
    private String ref2 ;
    FileWriter fileWriter ;
    StringBuilder str ;
    ServerSocket sock ;
    static String os ;
    static String pathc ;
    static String paths ;
    int status ;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton clientSelection;
    private javax.swing.JButton connetButton;
    private javax.swing.JTextField ipBox;
    private javax.swing.JButton jButton1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollBar jScrollBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea msgArea;
    private javax.swing.JTextField msgLine;
    private javax.swing.JTextField portBox;
    private javax.swing.JButton sendButton;
    private javax.swing.JRadioButton serverSelection;
    // End of variables declaration//GEN-END:variables
}
